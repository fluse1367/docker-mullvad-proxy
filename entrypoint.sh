#!/bin/bash

# check envs
if [ -z "$MULLVAD_SETTINGS_DIR" ]; then
    echo "MULLVAD_SETTINGS_DIR env not defined" >&2
    exit 1
fi

if [ -z "$ACCOUNT_NUMBER" ]; then
    echo "ACCOUNT_NUMBER env not defined" >&2
    exit 1
fi

set -m # enable job control

# start mullvad daemon in backgroud
mullvad-daemon -v &

# setup mullvad
#mullvad account login $ACCOUNT_NUMBER
mullvad relay set tunnel-protocol wireguard
mullvad lockdown-mode set on
mullvad lan set allow
mullvad auto-connect set on
mullvad account login $ACCOUNT_NUMBER
mullvad tunnel wireguard key regenerate
mullvad connect

# start nginx and tinyproxy into backgroud
nginx # nginx starts into backgroud by default
tinyproxy -d &
PID_TYPX="$!"

# put daemon in foreground
fg %mullvad # this will block until SIGTERM
# mullvad has terminated due to SIGTERM, stop nginx, tinyproxy and quit
nginx -s stop
kill -SIGTERM $PID_TYPX
tail --pid=$PID_TYPX -f /dev/null # wait for tinyproxy