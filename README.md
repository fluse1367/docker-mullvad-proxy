This is an image that provides a mullvad VPN connection through a SOCKS5/HTTP proxy.

This ubuntu based image runs nginx as socks5 reverse proxy and [tinyproxy](https://tinyproxy.github.io/) as http proxy under the hood.

### Run with plain docker
```
docker volume create mullvad-conf
docker network create -d bridge mullvad-net
docker run --rm -d --privileged \
    --cap-add NET_ADMIN --cap-add SYS_MODULE \
    --network mullvad-net \
    -v mullvad-conf:/conf \
    -e ACCOUNT_NUMBER=<Mullvad Account Number> \
    -p <Socks5 bind addr>:1080 \
    -p <HTTP bind addr>:8888 \
    fluse1367/mullvad-proxy
```

The bind addresses are in format `ip:port` or `port`. Recommanded binds:  
socks5: `127.10.0.1:1080`
http: `127.10.0.1:8888`

### Run with docker compose

**`.env`**
```.env
ACCOUNT_NUMBER=0000000000000000 # mullvad account number
SOCKS5_BIND=127.10.0.1:1080 # bind addr (ip:port|port) for the socks5 proxy
HTTP_BIND=127.10.0.1:8888 # bind add (ip:port|port) for the http proxy
```
*Instead of using the `.env` file, you can just replace the respective variables below.*

**`docker-compose.yml`**
```yml
services:
    mullvad:
        image: fluse1367/mullvad-proxy
        restart: unless-stopped
        networks: ['localnet']
        hostname: 'mullvad'
        ports: ["${SOCKS5_BIND}:1080", "${HTTP_BIND}:8888"]
        cap_add: ['NET_ADMIN', 'SYS_MODULE' ]
        sysctls:
            net.ipv4.conf.all.src_valid_mark: 1
        privileged: true
        environment: ["ACCOUNT_NUMBER=${ACCOUNT_NUMBER}"]
        volumes: ['mullvad-conf:/conf']
networks:
    localnet:
        driver: bridge
volumes:
    mullvad-conf:
```