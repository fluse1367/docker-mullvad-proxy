FROM ubuntu:20.04

RUN apt-get -y update
RUN apt-get -y install dbus curl
RUN echo "exit 0" > /usr/bin/systemctl && chmod a+x /usr/bin/systemctl

# install mullvad
RUN curl -sSLo /tmp/mv.deb https://mullvad.net/download/app/deb/latest && dpkg -i /tmp/mv.deb && rm -f /tmp/mv.dev

# install nginx
RUN apt-get -y install nginx-light libnginx-mod-stream
# remove all unnecessary nginx stuff
RUN find /etc/nginx -mindepth 1 | grep -vE '^.*(?=nginx\.conf|modules-enabled).*$' | while read l; do rm -rf $l; done
# copy our nginx config
COPY nginx.conf /etc/nginx/

# install tinyproxy
RUN apt-get -y install tinyproxy-bin
# copy tinyproxy config
COPY tinyproxy.conf /etc/tinyproxy/tinyproxy.conf

# clean apt
RUN apt-get -y clean
RUN rm -rf /var/lib/apt/lists/* 

ENV MULLVAD_SETTINGS_DIR=/conf
VOLUME $MULLVAD_SETTINGS_DIR

COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT /entrypoint.sh